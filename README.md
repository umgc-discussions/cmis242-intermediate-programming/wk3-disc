What is exception propagation? Give an example of a class that contains at least two methods, in which one method calls another. Ensure 
that the subordinate method will call a predefined Java method that can throw a checked exception. The subordinate method should not catch 
the exception. Explain how exception propagation will occur in your example.

Good Afternoon,

Exception propagation is a situation  which occurs when a method throws an unchecked exception. Unchecked exceptions consist of runtime exceptions, errors, and their subclasses. 

Once an unchecked exception is thrown, the runtime system begins looking for a way to handle that exception. It begins in the method which the exception originated in, and then proceeds down the call stack (basically going in reverse order of the methods called until it reaches main) looking for a handler for the exception thrown. 